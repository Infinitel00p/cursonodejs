
const axios = require('axios');

const getClima = async (lat,lng) => {
// https://api.openweathermap.org/data/2.5/weather?lat=35&lon=139&appid=f28df38d698eea8a86742a7bf0205374&units=metric

    let resp = await axios.get(`https://api.openweathermap.org/data/2.5/weather?lat=${ lat }&lon=${ lng }&appid=f28df38d698eea8a86742a7bf0205374&units=metric`)
    

    return resp.data.main.temp;
}

module.exports = {
    getClima
}