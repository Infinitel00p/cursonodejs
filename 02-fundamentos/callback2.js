let empleados = [{
    id:1, nombre: 'Francisco'
},{
    id: 2, nombre: 'Juan'
}];

let salarios = [{
    id :1,
    salario:1000
}]

let getEmpleado = (id, callback) => {
    let empleadoDB = empleados.find(empleado => empleado.id === id)
    
    if (!empleadoDB){
        callback(`No existe empleado con el id ${id}`)
    }else{
        callback(null, empleadoDB);
    }
}

let getSalario = (empleado, callback)=>{
    let salario = salarios.find(salario => salario.id === empleado.id);

    if (!salario){
        callback(`El empleado ${empleado.nombre} no tiene salario asignado`)
    }else{
        let emp = {
            nombre: empleado.nombre,
            salario: salario.salario
        }
        callback(null, emp);
    }
};

getEmpleado(1, (err, empleado)=>{
    if (err){
        return console.log(err);
    }
    console.log(empleado);   
    
    getSalario(empleado, (err, resp)=>{
        if (err){
            return console.log(err);
        }
        console.log(resp);   
            
    });
});
