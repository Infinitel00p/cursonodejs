const express = require('express');
const mongoose = require('mongoose');
const app = express();
const config = require('./config/config');
const bodyParser = require('body-parser');
const path = require('path');


// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }));
 
// parse application/json
app.use(bodyParser.json());
 
//Configuracion global de routes
app.use( require('./routes/index') );

//habilitar la carpeta public 
app.use( express.static( path.resolve(__dirname, '../public') ) );

// conexion a la base de datos
mongoose.connect(process.env.URLDB, { useNewUrlParser: true }, (err, res) => {
    if (err) throw err;

    console.log("DB online");
});
 
app.listen(process.env.PORT, () => {
    console.log('Escuchando puerto 3000');
})