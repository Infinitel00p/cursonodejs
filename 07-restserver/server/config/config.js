/* =================== 
    PUERTO
======================*/

process.env.PORT = process.env.PORT || 3000; 

/* =================== 
    Entorno
======================*/
process.env.NODE_ENV = process.env.NODE_ENV || 'dev';


/* =================== 
    Vencimiento del token
======================*/
process.env.CADUCIDAD_TOKEN = '48h';

/* =================== 
    SEED de autenticación
======================*/
process.env.SEED = process.env.SEED || 'semilla';

/* =================== 
    Base de datos
======================*/
let urlDB;
if (process.env.NODE_ENV === 'dev' ) 
    urlDB = 'mongodb://localhost:27017/cafe'; 
else    
    urlDB = 'mongodb://franc_ev:F123456@ds125932.mlab.com:25932/cafedata'

process.env.URLDB = urlDB;


/* =================== 
    Google CLIENT ID
======================*/
process.env.CLIENT_ID = process.env.CLIENT_ID || '829135030413-9dmgtijg9cau3n9q76nsamnbr4498mg4.apps.googleusercontent.com';