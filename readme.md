﻿usar este tipo de sintaxis (template literales) para concatenar strings console.log(`Hola ${ nombre }`);

Si se ejecuta ```npm intall``` se instalan todas las librerias de la depenencias 

para estar pendeinte de los cambios que es hagan en los arrchvos ``` nodemon server -e js,hbs,html.css ```


Para animaciones css usar WOW.js y animated.css

para instalr como desarrollo
npm intall --save-dev nodemons
carpeta fundametos
    -> destructuracion de obejetos
    -> funciones flecha
    -> 
carpeta 03-bases-node
    -> app.js obtener parametros desde terminal
    -> multiplicar.js abrir archivo y escrbir  en el

    instalar ##YARG para manejo de parametros por terminal
        -> npm init -para la creaccion del package.json -name debe ser url friendly 
        -> npm i yargs --
        
    usar ##colors para dar color en la terminal
        -> npm install colors --save

aplicaión del clima 
    -> cosumo de apis, peticiones http,
    -> libreria request
    -> libreria axios, usa promesas
        -> npm install axios --save

web server - /06-web-server
    Uso y configuración de Express
    Servir contenido estático
    Template engines
    Handebars
        Helpers
        Parciales
        Variables
    Despliegues en Heroku y Github
    Hacer carpetas públicas en la web

    -> para instalar ```npm install express --save```
        libreria para servidor
    
    -> npm hbs motor de templates | handlebars para contenido      dinamico
        ``` npm install hbs --save ```

rest server - /07-rest-server
    Instalación y pruebas con MongoDB
        
    Peticiones HTTP
        Get
        Put
        Post
            -> manejo del body (datos enviados) ```npm i body-parser --server```
        Delete
    Aprender sobre códigos de error HTTP
    Códigos de error en Express
    Archivos para la configuración global
    Tips importantes en Postman

    ```npm install mongoose --save``` instalciacin para manejar mongo desde nodejs
    ```npm i mongoose-unique-validator --save``` para manejar las validadciones unique key
    
    ````npm i bcrypt --save```` para encriptar contraseñas

|   ````npm install underscore --save````   
        -> pick() se usa para devolver un objeto filtrado
        -> mLab servicio de mongo en la nube
    
    ````npm install jsonwebtoken --save``` generar tokens
    
    Autenticacion con google
        ````npm install google-auth-library --save``` libreria de google para validar tokens

        
    ````npm i express-fileupload --save``` libreria para subir archivos con express


sockets  08-socket-basico
    Introducción a los sockets
    Resolver preguntas comunes sobre los sockets
    Instalación de Socket.io
    Detectar conexiones y desconexiones de usuarios
    Emitir mensajes cliente servidor / servidor cliente
    Escuchar los mensajes servidor cliente / cliente servidor
    Broadcast
    Callbacks en los sockets
    Pruebas en Heroku

    ``npm i socket.io --save`` instalar libreria para ckets

sockets 10-socket-chat
    Comunicación entre usuarios
    Comunicación entre 1 a muchos
    Comunicación 1 a 1
    Comunicación entre salas de chat
    Diseño del chat
    Diseño del login
    Notificaciones de entrada de usuarios y salida de usuarios

	envio de archivos por socketio
	https://code.tutsplus.com/tutorials/how-to-create-a-resumable-video-uploader-in-nodejs--net-25445


typescript 11-type-scriptt
    usar tpescritp con node 
    ```tsc --init`` para iniciar un proyecto con tyscript. genera el archivo tsconfig.json
    ``tsc`` comando para compilar el ts a js
    ``npm install @types/<nombre de paquete js> --save-dev``  ayuda para el tipado de los paquetes js en ts
    ```npm i copyfiles --save-dev`` para instalar paquete que manejoe la creacion de folders dentro de dist. ejecutar dentro del PACKAGE:json -> scripts -> `` "html":"copyfiles ./public/*.html ./dist `` 
    

    instalar mysql para node
        -> ``npm install mysql``
        -> ``npm install mysqljs/mysql``
        -> ```npm install @types/mysql --save-dev```

    Informacion sobre typesacript
    http://www.typescriptlang.org/docs/home.html