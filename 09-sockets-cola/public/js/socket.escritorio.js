var socket = io();

//comando para establecer la conexion
var socket = io();

var searchParams = new URLSearchParams( window.location.search);
if (!searchParams.has('escritorio')){
    window.location = 'index.html';
    throw new Error('El escritorio es necesario');
}

var escritorio = searchParams.get('escritorio');

$('h1').text('Escritorio '+escritorio);
var label = $('small');

$('button').on('click', function(){
    socket.emit('atenderTicket', {
        escritorio: escritorio
    }, function(res){
        if (res === 'No hay tickets'){
            label.text(res);
            alert(res);
            return;
        }
        label.text('ticket '+res.numero);
    })
})

socket.on('connect', function(){
    console.log('Conectado al servidor');
});

socket.on('disconnect', function(){
    console.log('Desconectado del servidor');
});

