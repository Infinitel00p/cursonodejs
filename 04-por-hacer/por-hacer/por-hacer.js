

const fs = require ('fs');

let listadoPorHacer = [];

const guardarDB = () => {
    let data = JSON.stringify(listadoPorHacer);
    fs.writeFile(`db/data.json`, data, (err) => {
        if (err) throw new Error('error al guardar el archivo');
    });
}

const cargarDB = () => {
    try {
        listadoPorHacer = require('../db/data.json');    
    } catch (error) {
        listadoPorHacer = []
    }

    // console.log(listadoPorHacer);
}

const crear = (descripcion) => {
    
    let porHAcer = {
        descripcion,
        completado: false
    };

    cargarDB();

    listadoPorHacer.push(porHAcer);

    guardarDB();

    return porHAcer;
}

const getListado =  () => {
    cargarDB();
    return listadoPorHacer; 
}

const actualizar = (descripcion, completado= true) => {
    cargarDB();
    
    let index = listadoPorHacer.findIndex( tarea => tarea.descripcion === descripcion); 

    if (index >= 0){
        listadoPorHacer[index].completado = completado;
        guardarDB();
        return true;
    } 
    else false;
}

const borrar = (descripcion) => {
    cargarDB();

    let nuevoListado = listadoPorHacer.filter( tarea => tarea.descripcion !== descripcion);

    if ( listadoPorHacer.length === nuevoListado) return false
    else {
        listadoPorHacer = nuevoListado;
        guardarDB();
        return true;
    }
}

module.exports = {
    crear,
    actualizar,
    getListado,
    borrar
}