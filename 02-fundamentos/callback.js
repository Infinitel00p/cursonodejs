let getUsuarioById = (id, callback) => {
    let usuario = {
        nombre: 'Francisco',
        id
    }

    if (id == 20){
        callback(`EL usuario con id ${20}`);
    }else{
        callback(null, usuario);
    }
}

getUsuarioById(2, (err, usuario)=>{
    if ( err ){
        return console.log("error");
    }
    console.log('Usuario de base de datos', usuario);
})