let empleados = [{
    id:1, nombre: 'Francisco'
},{
    id: 2, nombre: 'Juan'
}];

let salarios = [{
    id :1,
    salario:1000
}]

let getEmpleado = async  (id) => {

    let empleadoDB = empleados.find(empleado => empleado.id === id)
    
    if (!empleadoDB){
        throw new Error(`No existe empleado con el id ${id}`)
    }else{
        return empleadoDB;
    }

}

let getSalario = async (empleado) => {

    let salario = salarios.find(salario => salario.id === empleado.id);

    if (!salario){
        throw new Error(`El empleado ${empleado.nombre} no tiene salario asignado`)
    }else{
        let emp = {
            nombre: empleado.nombre,
            salario: salario.salario
        }
        return emp;
    }

};

let getinformacion = async (id) => {
    let empleado = await getEmpleado(id);
    let resp = await getSalario(empleado);

    return `${resp.nombre} tiene un salario de ${resp.salario}$`;
}

getinformacion(2)
    .then(mensaje => console.log(mensaje))
    .catch(err => console.log(err));

//getEmpleado(1, (err, empleado)=>{
//   if (err){
//        return console.log(err);
//    }
//    console.log(empleado);   

//    getSalario(empleado, (err, resp)=>{
//        if (err){
//            return console.log(err);
//        }
//        console.log(resp);   
            
//    });
//});