var socket = io();

        socket.on('connect', function(){
            console.log('Conectado al servidor');
        });

        //escuchar
        socket.on('disconnect', function(){
            console.log('Perdimos conexión con el servidor');
        });

        // enviar información 
        socket.emit('enviarMensaje', {
            usuario: 'Fernando',
            mensaje: 'Hola mundo'
        }, function( res ){
            console.log('respuesta del servidor: ',res);
        });

        // escuchar información
        socket.on('enviarMensaje', function(mensaje){
            console.log(mensaje);
        });