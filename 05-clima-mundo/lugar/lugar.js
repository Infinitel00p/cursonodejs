const axios = require('axios');

const getLugarLatLng = async (direccion) => {

    let encodeUrl = encodeURI(direccion);

    let resp = await axios.get(`https://maps.googleapis.com/maps/api/geocode/json?address=${ encodeUrl }`)
    
    if (resp.data.status === 'INVALID_REQUEST'){
    let encodeUrl = encodeURI(direccion);
        throw new Error(`No hay resultado para la ciudad ${direccion}`)
    }
        
    //console.log( JSON.stringify(resp.data, undefined, 2));
    let location = resp.data.results[0];
    let coors = location.geometry.location;

    return {
        direccion: location.formatted_address,
        lat: coors.lat,
        lng: coors.lng
    }
}

module.exports = {
    getLugarLatLng
}