const express = require('express');
const fileUpload = require('express-fileupload');
const app = express();

const {verificaTokenImg} = require('../middlewares/autenticacion')

const fs = require('fs');
const path = require('path');

app.get('/imagen/:tipo/:img', verificaTokenImg, (req, res) =>{
    
    let tipo = req.params.tipo;
    let img = req.params.img;

    //validar tipo
    let tiposValidos = ['productos', 'usuarios'];
    if (tiposValidos.indexOf(tipo) < 0){
        return res.status(400).json({
            ok: false,
            err:{
                message: 'Los tipos permitidas son ' + tiposValidos.join(', '),
            }
        });
    }

    let pathImage = path.resolve(__dirname, `../../uploads/${ tipo }/${ img }`);
    if ( fs.existsSync(pathImage) )    
        res.sendFile(pathImage);
    else{
        let noImgePath = path.resolve(__dirname, '../assets/no-image.jpg')
        res.sendFile(noImgePath);
    }
})

module.exports = app;