const express = require('express');
const Producto = require('../models/producto');
const app = express();
const { verificaToken, verificaAdmin_role } = require('../middlewares/autenticacion');

// ==============================
//  mostrar todas los productos
// ==============================
app.get('/productos', (req, res) => {
    // trae todos los productos
    // populate: usuario categoria
    // paginado

    let desde = req.query.desde || 0;
    desde = Number (desde);

    producto.find({disponible: true})
        .populate('usuario', 'nombre email')
        .populate('categoria')
        .skip(desde)
        .limit(5)
        .sort('descripcion')
        .exec((err, prodcutos) => {
            if ( err ){
                return res.status(500).json({
                    ok: false,
                    err
                })
            }

            res.json({
                ok: true,
                prodcutos
            })
        });
});


// ==============================
//  obtener producto por id
// ==============================
app.get('/productos/:id',verificaToken, (req, res) => {
    // Categoria.findBydId()
    let id = req.params.id;
    
    Producto.findById(id)
        .populate('usuario', 'nombre email')
        .populate('categoria')
        .exec((err, prodcutoDB) => {
            if ( err ){
                return res.status(500).json({
                    ok: false,
                    err
                })
            }

            if (!prodcutoDB){
                return res.status(400).json({
                    ok: false,
                    err: {
                        message: 'El producto no existe'
                    }
                })
            }

            res.json({
                ok: true,
                prodcuto: prodcutoDB
            })
        });
    
});


// ==========================
//  Buscar  productos
//  por terminos en buscador
// ==========================
app.get('/productos/bucar/:termino', verificaToken, (req, res) => {
    let termino = req.params.termino;

    let regex = new RegExp(termino, 'i');

    Producto.find({nombre: regex})
        .populate(categoria)
        .exec( (err, res) => {
            if ( err ){
                return res.status(500).json({
                    ok: false,
                    err
                })
            }

            if (!prodcutoDB){
                return res.status(400).json({
                    ok: false,
                    err: {
                        message: 'El producto no existe'
                    }
                })
            }

            res.json({
                ok: true,
                prodcuto: prodcutoDB
            })
        })
})

// ========================
//  crear nueva productos
// ========================
app.post('/productos', verificaToken, (req, res) => {
    // regresar nueva categoria

    let body = req.body;

    let producto = new Producto({
        usuario: req.usuario._id,
        nombre: body.nombre,
        precioUni: body.precioUni,
        descripcion: body.descripcion,
        disponible: body.disponible,
        categoria: body.categoria
    })

    producto.save( (err, productoDB) => {
        if ( err ){
            return res.status(500).json({
                ok: false,
                err
            })
        }

        if (!productoDB){
            if ( err ){
                return res.status(400).json({
                    ok: false,
                    err
                })
            }
        }

        return res.status(201).json({
            ok: true,
            producto: productoDB
        })
    });
});



// ========================
//  actualizar  producto
// ========================
app.put('/productos/:id', verificaToken, (req, res) => {
    
    let id = req.params.id;
    let body = req.body;
    
    Categoria.findById(id, (err, res) => {
        if ( err ){
            return res.status(500).json({
                ok: false,
                err
            })
        }

        if (!productoDB){
            if ( err ){
                return res.status(400).json({
                    ok: false,
                    err
                })
            }
        }

        productoDB.nombre = body.nombre;
        productoDB.precioUni = body.precioUni;
        productoDB.categoria = body.categoria;
        productoDB.disponible = body.disponible;
        productoDB.descripcion = body.descripcion;

        productoDB.save ( (err, productoGuardado) => {
            if (err){
                return res.status(500).json({
                    ok:false,
                    err
                })
            }

            res.json( {
                ok:true,
                producto: productoGuardado
            })

        });

    });
});


// ========================
//  eliminar producto
// ========================
app.delete('/productos/:id', [verificaToken, verificaAdmin_role], (req, res) => {
    // solo un administrador pede borrar categorias

    let id = req.params.id;

    Producto.findById( id, (err, productoBD) => {
        if ( err ){
            return res.status(500).json({
                ok: false,
                err
            })
        }

        if (!productoBD){
            if ( err ){
                return res.status(400).json({
                    ok: false,
                    err: {
                        message: ' EL id no existe'
                    }
                })
            }

        }

        productoDB.disponible = false;
        producto.save ( (err, productoDB) => {
            if ( err ){
                return res.status(400).json({
                    ok: false,
                    err: {
                        message: ' EL id no existe'
                    }
                })
            }
        })

        res.status(400).json({
            ok: false,
            producto: productoBD,
            err: {
                message: ' Producto borrado'
            }
        })
    })
});

module.exports = app;
