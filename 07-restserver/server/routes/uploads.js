const express = require('express');
const fileUpload = require('express-fileupload');
const app = express();

const Usuario = require('../models/usuario');
const Producto = require('../models/Producto');

const fs = require('fs');
const path = require('path');
 
// default options
app.use(fileUpload());

app.put('/upload/:tipo/:id', (req, res) => {
    let tipo = req.params.tipo;
    let id = req.params.id;

    //validar tipo
    let tiposValidos = ['productos', 'usuarios'];
    if (tiposValidos.indexOf(tipo) < 0){
        return res.status(400).json({
            ok: false,
            err:{
                message: 'Los tipos permitidas son ' + tiposValidos.join(', '),
            }
        });
    }
    
    console.log(id);
    console.log(req.files);
    
    if (!req.files){
        return res.status(400).json({
            ok: false,
            err:{
                message: 'Ningún archivo ha sido subido.'
            }
        });
    }
    // The name of the input field (i.e. "sampleFile") is used to retrieve the uploaded file
    let archivo = req.files.archivo;

    //Extensiones permitidas
    let extensionesValidas = ['png', 'jpg', 'gif', 'jpeg'];
    let nombreCortado = archivo.name.split('.');
    let extension = nombreCortado[nombreCortado.length -1];

    if (extensionesValidas.indexOf(extension) < 0){
        return res.status(400).json({
            ok: false,
            err:{
                message: 'Las extensiones permitidas son ' + extensionesValidas.join(', '),
                extension
            }
        })
    }

    //Cambiar nombre del archivo
    let nombreArchivo = `${ id }-${ new Date().getMilliseconds }.${ extension }`;

    // Use the mv() method to place the file somewhere on your server
    archivo.mv(`uploads/${ tipo }/${ nombreArchivo }`, (err) => {
        if (err)
            return res.status(500).json({
                ok: false,
                err    
            });

        // Aqui, Imagen Cargada

        if (tipo === 'usuarios' )
            imagenUsuario(id, res, nombreArchivo);
        else
            imagenProducto(id, res, nombreArchivo);


    });
 
 
})

function imagenUsuario(id, res, nombreArchivo) {
    Usuario.findById(id, (err, usuarioDB) => {
        if (err){

            borraArchivo(nombreArchivo, 'usuarios');

            return res.status(500).json({
                ok: true,
                err
            })
        }

        if ( !usuarioDB){
            borraArchivo(nombreArchivo, 'usuarios');
            
            return res.status(500).json({
                ok: true,
                err: {
                    message: 'Usuario no existe'
                }
            })
        }

        

        usuarioDB.img = nombreArchivo;

        usuarioDB.save ((err, usuarioGuardado) => {
            res.json({
                ok:true,
                usuarios: usuarioGuardado,
            })
        })
    })
}

function imagenProducto(id, res, nombreArchivo) {
    Producto.findById(id, (err, productoDB) => {
        if (err){

            borraArchivo(nombreArchivo, 'productos');

            return res.status(500).json({
                ok: true,
                err
            })
        }

        if ( !productoDB){
            borraArchivo(nombreArchivo, 'productos');
            
            return res.status(500).json({
                ok: true,
                err: {
                    message: 'Producto con ese id no existe'
                }
            })
        }

        productoDB.img = nombreArchivo;

        productoDB.save ((err, productoGuardado) => {
            res.json({
                ok:true,
                usuarios: productoGuardado,
            })
        })
    })
}

function borraArchivo(nombreImagen, tipo) {
    let pathImage = path.resolve(__dirname, `../../uploads/${ tipo }/${ nombreImagen }`);
    if ( fs.existsSync(pathImage) ){
        fs.unlinkSync(pathImage);
    }
}



module.exports = app;