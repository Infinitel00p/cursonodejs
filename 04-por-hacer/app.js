

const argv = require('./config/yargs').argv;
const colors = require('colors');
const porHacer = require('./por-hacer/por-hacer');

let comando = argv._[0];

// console.log(argv);

switch (comando) {
    case 'crear':
        console.log('Crea una nueva tarea por hacer');
        let tarea = porHacer.crear(argv.descripcion);
        console.log(tarea);
        break;

    case 'listar':
        console.log('Listar tarea por hacer');

        let listado = porHacer.getListado();

        for (let tarea of listado) {
            console.log('===== por hacer ====='.green);
            console.log(tarea.descripcion);
            console.log('Estado: ', tarea.completado);
            console.log('====================='.green);
        }
        break;

    case 'actualizar':
        console.log('Actualizar tarea por hacer');
        let act = porHacer.actualizar(argv.descripcion, argv.completado);
        console.log(act); 

        break;

    case 'borrar':
        let borrado = porHacer.borrar(argv.descripcion);
        console.log(borrado);
        break;

    default:
        console.log('Comando no reconocido');
        break;
}