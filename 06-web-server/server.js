

const express = require('express');
const app = express();
const hbs = require('hbs');
require('./hbs/helpers');

const port = process.env.port || 3000;

app.use( express.static( __dirname + '/public') );

hbs.registerPartials(__dirname + '/views/partials');

app.set('view engine', 'hbs');

//helpers


app.get('/', (req, res) => {
    res.render('home',{
        nombre: 'Francisco'
    });
})

app.get('/about', (req, res) => {
    res.render('about',{
        nombre: 'Francisco'
    });
})


app.get('/api/home', (req, res) => {
    let salida = {
        nombre: 'fernando',
        edad: 32,
        url: req.url
    }

    // res.send('Hola mundo')
    res.send(salida);
})
 
app.listen(port, () => {
    console.log(`Escando peticiones en el puerto ${ port } `);
});