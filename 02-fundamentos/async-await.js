// con async devuelve una promesa con el return de la función
let getNombre = () => {
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            resolve('Francisco');
        }, 3000);
    })
};

let saludo = async () => {
    let nombre = await getNombre();

    return `Hola ${nombre}`;
}

//console.log(getNombre());

saludo().then(mensaje => {
    console.log(mensaje);
}).catch(err => {
    console.log("Hubo un error");
})
