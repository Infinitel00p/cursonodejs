const express = require('express');
const Categoria = require('../models/categoria');
const app = express();
const { verificaToken, verificaAdmin_role } = require('../middlewares/autenticacion');

// ==============================
//  mostrar todas las categorias
// ==============================
app.get('/categoria', (req, res) => {
    Categoria.find({})
        .populate('usuario', 'nombre email')
        .sort('descripcion')
        .exec((err, categorias) => {
            if ( err ){
                return res.status(500).json({
                    ok: false,
                    err
                })
            }

            res.json({
                ok: true,
                categorias
            })
        });
});


// ==============================
//  mostrar una categoria por id
// ==============================
app.get('/categoria/:id',verificaToken, (req, res) => {
    // Categoria.findBydId()
    let id = req.params.id;
    Categoria.findById(id)
        .exec((err, categoriaDB) => {
            if ( err ){
                return res.status(500).json({
                    ok: false,
                    err
                })
            }

            if (!categoriaDB){
                return res.status(400).json({
                    ok: false,
                    err: {
                        message: 'La categoria no existe'
                    }
                })
            }

            res.json({
                ok: true,
                categoria: categoriaDB
            })
        });
    
});



// ========================
//  crear nueva categoría
// ========================
app.post('/categoria', verificaToken, (req, res) => {
    // regresar nueva categoria

    let body = req.body;

    let categoria = new Categoria({
        descripcion: body.descripcion,
        usuario: req.usuario._id,
    })

    categoria.save( (err, categoriaDB) => {
        if ( err ){
            return res.status(500).json({
                ok: false,
                err
            })
        }

        if (!categoriaDB){
            if ( err ){
                return res.status(400).json({
                    ok: false,
                    err
                })
            }
        }

        return res.json({
            ok: true,
            categoria: categoriaDB
        })
    });
});



// ========================
//  crear nueva categoría
// ========================
app.put('/categoria/:id', verificaToken, (req, res) => {
    // regresar nueva categoria

    let id = req.params.id;
    let body = req.body;
    
    let descCategoria = {
        descripcion:body.descripcion
    }

    Categoria.findByIdAndUpdate(id, descCategoria, {new: true, runValidators: true}, (err, res) => {
        if ( err ){
            return res.status(500).json({
                ok: false,
                err
            })
        }

        if (!categoriaDB){
            if ( err ){
                return res.status(400).json({
                    ok: false,
                    err
                })
            }
        }

        return res.json({
            ok: true,
            categoria: categoriaDB
        })
    });
});


// ========================
//  crear nueva categoría
// ========================
app.delete('/categoria/:id', [verificaToken, verificaAdmin_role], (req, res) => {
    // solo un administrador pede borrar categorias

    let id = req.params.id;
    Categoria.findByIdAndRemove( id, (err, categoriaBD) => {
        if ( err ){
            return res.status(500).json({
                ok: false,
                err
            })
        }

        if (!categoriaBD){
            if ( err ){
                return res.status(400).json({
                    ok: false,
                    err: {
                        message: ' EL id no existe'
                    }
                })
            }

        }

        
        res.json({
            ok: true,
            categorias: categoriaBD,
            message: 'Categoría Borrada'
        });
    })
});

module.exports = app;
