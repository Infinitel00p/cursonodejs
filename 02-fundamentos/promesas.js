let empleados = [{
    id:1, nombre: 'Francisco'
},{
    id: 2, nombre: 'Juan'
}];

let salarios = [{
    id :1,
    salario:1000
}]

let getEmpleado = (id) => {

    return new Promise((resolve, reject) => {
        let empleadoDB = empleados.find(empleado => empleado.id === id)
        
        if (!empleadoDB){
            reject(`No existe empleado con el id ${id}`)
        }else{
            resolve(empleadoDB);
        }

    });

}

let getSalario = (empleado) => {

    return new Promise((resolve, reject) => {
        let salario = salarios.find(salario => salario.id === empleado.id);

        if (!salario){
            reject(`El empleado ${empleado.nombre} no tiene salario asignado`)
        }else{
            let emp = {
                nombre: empleado.nombre,
                salario: salario.salario
            }
            resolve(emp);
        }
    })
    
};


getEmpleado(3).then(empleado => {
    console.log('empleado de DB', empleado);

    return getSalario(empleado);
})
.then( resp => {
    console.log(resp);
})
.catch(err => {
    console.log(err);
});